#include <stdint.h>
#include <string.h>

#include <Wire.h>

#include "raat.hpp"
#include "raat-buffer.hpp"

#include "raat-oneshot-timer.hpp"
#include "raat-oneshot-task.hpp"
#include "raat-task.hpp"

#include "http-get-server.hpp"

typedef struct _timeout
{
    int32_t time;
    bool active;
} timeout;

static HTTPGetServer s_server(NULL);
static const raat_devices_struct * s_pDevices = NULL;
static const raat_params_struct * s_pParams = NULL;

#if NRELAYS == 2
#elif NRELAYS == 4
#elif NRELAYS == 8
#else
#error "NRELAYS expected to be 2, 4 or 8"
#endif

#define MAX_OUTPUT (MIN_OUTPUT + NRELAYS - 1)

static bool s_output_states[NRELAYS];
static timeout s_timeouts[NRELAYS];

static bool bSerialMode = false;

typedef void (*output_setter_fn)(int32_t);

static void send_response(HTTPGetServer& server, char const * const pResponse)
{
    if (bSerialMode)
    {
        raat_logln(LOG_APP, pResponse);
        bSerialMode = false;
    }
    else
    {
        server.add_body(pResponse);
    }
}

static void send_response_P(HTTPGetServer& server, char const * const pResponse)
{
    if (bSerialMode)
    {
        raat_logln_P(LOG_APP, pResponse);
        bSerialMode = false;
    }
    else
    {
        server.add_body_P(pResponse);
    }
}

static void do_set(int32_t output_pin)
{
    raat_logln_P(LOG_APP, PSTR("Setting output %" PRIi32), output_pin);
    s_output_states[output_pin-MIN_OUTPUT] = true;
    s_timeouts[output_pin-MIN_OUTPUT].active = false;
    digitalWrite(output_pin, HIGH);
}

static void do_toggle(int32_t output_pin)
{
    raat_logln_P(LOG_APP, PSTR("Toggling output %" PRIi32), output_pin);
    s_output_states[output_pin-MIN_OUTPUT] = !s_output_states[output_pin-MIN_OUTPUT];
    s_timeouts[output_pin-MIN_OUTPUT].active = false;
    digitalWrite(output_pin, s_output_states[output_pin-MIN_OUTPUT] ? HIGH : LOW);
}

static void do_clear(int32_t output_pin)
{
    raat_logln_P(LOG_APP, PSTR("Clearing output %" PRIi32), output_pin);
    s_output_states[output_pin-MIN_OUTPUT] = false;
    s_timeouts[output_pin-MIN_OUTPUT].active = false;
    digitalWrite(output_pin, LOW);
}

static void start_timeout(int32_t timeout, int32_t output_pin)
{
    s_timeouts[output_pin-MIN_OUTPUT].time = (timeout / 100) * 100;
    s_timeouts[output_pin-MIN_OUTPUT].active = true;
    raat_logln_P(LOG_APP, PSTR("Starting %" PRIi32 " timeout on output %" PRIi32),
        s_timeouts[output_pin-MIN_OUTPUT].time, output_pin
    );
}

static void send_standard_erm_response(HTTPGetServer& server)
{
    if (!bSerialMode)
    {
        server.set_response_code_P(PSTR("200 OK"));
        server.set_header_P(PSTR("Access-Control-Allow-Origin"), PSTR("*"));
        server.finish_headers();
    }
}

static void get_input(HTTPGetServer& server, char const * const url, char const * const end)
{
    (void)url;

    int32_t input_pin;
    char const * const pInputPin = end;

    bool success = false;

    send_standard_erm_response(server);

    if ((success = raat_parse_single_numeric(pInputPin, input_pin, NULL)))
    {
        success = ((input_pin >= MIN_INPUT) && (input_pin <= MAX_INPUT));

        if (success)
        {
            if (digitalRead(A0+input_pin) == HIGH)
            {
                send_response_P(server, PSTR("1\r\n\r\n"));
            }
            else
            {
                send_response_P(server, PSTR("0\r\n\r\n"));
            }
        }
    }

    if (!success)
    {
        send_response_P(server, PSTR("?\r\n\r\n"));
    }
}

static void set_bool_response(HTTPGetServer& server, bool success)
{
    if (success)
    {
        send_response_P(server, PSTR("OK\r\n\r\n"));
    }
    else
    {
        send_response_P(server, PSTR("?\r\n\r\n"));
    }
}

static void nontimed_output_handler(HTTPGetServer& server, char const * const pOutputUrl, output_setter_fn pOutputSetterFn)
{
    int32_t output_pin;

    bool success = false;

    send_standard_erm_response(server);

    if ((success = raat_parse_single_numeric(pOutputUrl, output_pin, NULL)))
    {
        success = ((output_pin >= 2) && (output_pin <= (NRELAYS+2)));
        if (success)
        {
            pOutputSetterFn(output_pin);
        }
    }
    else
    {
        raat_logln_P(LOG_APP, PSTR("Could not parse %s"), pOutputUrl);
    }
    set_bool_response(server, success);
}

static void timed_output_handler(HTTPGetServer& server, char const * const pOutputUrl, output_setter_fn pOutputSetterFn)
{
    int32_t output_pin;
    int32_t timeout;

    char * pTime = NULL;

    bool success = false;

    send_standard_erm_response(server);

    if ((success = raat_parse_single_numeric(pOutputUrl, output_pin, &pTime)))
    {
        if ((success = raat_parse_single_numeric(pTime+1, timeout, NULL)))
        {
            success = ((output_pin >= 2) && (output_pin <= (NRELAYS+2)));
            success &= timeout > 100;

            if (success)
            {
                pOutputSetterFn(output_pin);
                start_timeout(timeout, output_pin);
            }
        }
        else
        {
            raat_logln_P(LOG_APP, PSTR("Could not parse %s"), pTime+1);
        }
    }
    else
    {
        raat_logln_P(LOG_APP, PSTR("Could not parse %s"), pOutputUrl);
    }

    set_bool_response(server, success);
}

static void set_output(HTTPGetServer& server, char const * const url, char const * const end)
{
    (void)server; (void)url;
    nontimed_output_handler(server, end+1, do_set);
}

static void toggle_output(HTTPGetServer& server, char const * const url, char const * const end)
{
    (void)server; (void)url;
    nontimed_output_handler(server, end+1, do_toggle);
}

static void clear_output(HTTPGetServer& server, char const * const url, char const * const end)
{
    (void)server; (void)url;
    nontimed_output_handler(server, end+1, do_clear);
}

static void timed_set_output(HTTPGetServer& server, char const * const url, char const * const end)
{
    (void)server; (void)url;
    timed_output_handler(server, end+1, do_set);
}


static void timed_toggle_output(HTTPGetServer& server, char const * const url, char const * const end)
{
    (void)server; (void)url;
    timed_output_handler(server, end+1, do_toggle);
}

static void timed_clear_output(HTTPGetServer& server, char const * const url, char const * const end)
{
    (void)server; (void)url;
    timed_output_handler(server, end+1, do_clear);
}

static void name_url(HTTPGetServer& server, char const * const url, char const * const end)
{
    (void)server; (void)url; (void)end;

    if (*end == '/')
    {
        raat_logln_P(LOG_APP, PSTR("New name: %s"), end+1);
        s_pParams->pApplicationName->set(end+1);
    }

    char const * const pName = s_pParams->pApplicationName->get();

    raat_logln_P(LOG_APP, PSTR("Handling app name request (%s)"), pName);

    send_standard_erm_response(server);

    send_response(server, pName);
    send_response_P(server, PSTR("\r\n\r\n"));
}

static const char GET_INPUT_URL[] PROGMEM = "/input/get";
static const char SET_OUTPUT_URL[] PROGMEM = "/output/set";
static const char TOGGLE_OUTPUT_URL[] PROGMEM = "/output/toggle";
static const char CLEAR_OUTPUT_URL[] PROGMEM = "/output/clear";
static const char TIMED_SET_OUTPUT_URL[] PROGMEM = "/output/timedset";
static const char TIMED_TOGGLE_OUTPUT_URL[] PROGMEM = "/output/timedtoggle";
static const char TIMED_CLEAR_OUTPUT_URL[] PROGMEM = "/output/timedclear";
static const char NAME_URL[] PROGMEM = "/app/name";

static http_get_handler s_handlers[] =
{
    {GET_INPUT_URL, get_input},
    {SET_OUTPUT_URL, set_output},
    {TOGGLE_OUTPUT_URL, toggle_output},
    {CLEAR_OUTPUT_URL, clear_output},
    {TIMED_SET_OUTPUT_URL, timed_set_output},
    {TIMED_TOGGLE_OUTPUT_URL, timed_toggle_output},
    {TIMED_CLEAR_OUTPUT_URL, timed_clear_output},
    {NAME_URL, name_url},
    {"", NULL}
};

void ethernet_packet_handler(char * req)
{
    s_server.handle_req(s_handlers, req);
}

char * ethernet_response_provider()
{
    return s_server.get_response();
}

static void timeout_task_fn(RAATTask& task, void * pTaskData)
{
    (void)task; (void)pTaskData;
    for (uint8_t output_pin = MIN_OUTPUT; output_pin <= MAX_OUTPUT; output_pin++)
    {
        if (s_timeouts[output_pin-MIN_OUTPUT].active && s_timeouts[output_pin-MIN_OUTPUT].time > 0)
        {
            s_timeouts[output_pin-MIN_OUTPUT].time -= 100;
            if (s_timeouts[output_pin-MIN_OUTPUT].time == 0)
            {
                do_toggle(output_pin);
            }
        }
    }
}
static RAATTask s_timeout_task(100, timeout_task_fn);

void raat_on_setup_start()
{
    /* Ensure pin 10 is an output to prevent SPI losing master mode.
     * See datasheet section 18.3.2:
     *     If SS is configured as an output, the pin is a general output pin which does not affect the SPI system. Typically, the pin will be
     *     driving the SS pin of the SPI slave.
     *     If SS is configured as an input, it must be held high to ensure master SPI operation.
     */
     if (NRELAYS == 8)
     {
         pinMode(10, OUTPUT);
     }
}

void raat_custom_setup(const raat_devices_struct& devices, const raat_params_struct& params)
{
    s_pDevices = &devices;
    s_pParams = &params;

    char default_on[9] = "";
    params.pDefaultOn->get(default_on, 8);

    for (uint8_t output_pin = MIN_OUTPUT; output_pin <= MAX_OUTPUT; output_pin++)
    {
        pinMode(output_pin, OUTPUT);
        if (default_on[output_pin-MIN_OUTPUT] == '1')
        {
            do_set(output_pin);
        }
        else
        {
            do_clear(output_pin);
        }
        s_timeouts[output_pin-MIN_OUTPUT].time = 0;
        s_timeouts[output_pin-MIN_OUTPUT].active = false;
    }

    for (uint8_t i = MIN_INPUT; i <= MAX_INPUT; i++)
    {
        pinMode(i, INPUT_PULLUP);
    }

    raat_logln_P(LOG_APP, PSTR("Simple Nano Ethernet: ready"));
}

void raat_custom_loop(const raat_devices_struct& devices, const raat_params_struct& params)
{
    (void)devices;
    s_timeout_task.run();

    if (params.pSerialURL->strlen())
    {
        static char url[40] = "";
        params.pSerialURL->get(url, 40);

        http_get_handler const * const pHandler = s_server.match_handler_url(url, s_handlers);

        if (pHandler)
        {
            uint16_t handler_fixed_length = raat_board_strlen_progmem(pHandler->url);
            char const * const pEnd = url + handler_fixed_length;
            raat_logln_P(LOG_APP, PSTR("Handling URL '%s'."), url);
            bSerialMode = true;
            pHandler->fn(s_server, url, pEnd);
        }
        else
        {
            raat_logln_P(LOG_APP, PSTR("URL '%s' not found."), url);
        }
        params.pSerialURL->set("");
    }
}
