""" test.py

Usage:
	test.py <ip> <min_op> <max_op>

"""


import requests
import time
import docopt

if __name__ == "__main__":

	args = docopt.docopt(__doc__)

	ip = args["<ip>"]
	max_output = int(args["<max_op>"])
	min_output = int(args["<min_op>"])

	print(requests.get("http://{ip}/app/name".format(ip=ip)))

	while True:
		for op in range(min_output, max_output+1):
			print("Output {op} on".format(op=op))
			requests.get("http://{ip}/output/set/{op}".format(ip=ip, op=op))
			time.sleep(2)
			print("Output {op} off".format(op=op))
			requests.get("http://{ip}/output/clear/{op}".format(ip=ip, op=op))
			time.sleep(2)
