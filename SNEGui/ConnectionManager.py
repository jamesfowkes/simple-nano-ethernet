import logging
import threading

from PySide6 import QtCore

from StoppableThread import StoppableThread
from CheckableEvent import CheckableEvent
from nano import SerialNano

logger = logging.getLogger(__name__)

class ConnectionManager(QtCore.QObject, StoppableThread):

    connection_change_signal = QtCore.Signal(SerialNano)

    def __init__(self):
        StoppableThread.__init__(self)
        QtCore.QObject.__init__(self)

        self.nano = None
        self.connect_flag = CheckableEvent()
        self.disconnect_flag =  CheckableEvent()
        self.port = None

    def register_slot(self, slot):
        return self.connection_change_signal.connect(slot)

    def reset(self):
        self.port = None
        if self.nano:
            logger.info("Disconnecting serial nano")
            self.nano.close()
            self.nano = None
        self.connection_change_signal.emit(self.nano)

    def run(self):
        while self.run_flag:
            if self.connect_flag.check_and_clear():
                logger.info(f"Attempting to connect to device at {self.port}")
                try:
                    self.nano = SerialNano(self.port)
                except Exception as e:
                    logger.info(f"Exception '{e}' when connecting to {self.port}")
                    self.reset()

                if self.nano:
                    self.nano.update()
                    if self.nano.is_valid_device():
                        logger.info(f"Connected to device at {self.port}")
                        self.connection_change_signal.emit(self.nano)
                    else:
                        logger.info(f"Invalid device at {self.port}")
                        self.reset()

            if self.disconnect_flag.check_and_clear():
                self.reset()

        # On exit, clean up
        if self.nano:
            self.nano.close()

    def connect_nano(self, port):
        self.connect_flag.set()
        self.port = port

    def disconnect_nano(self):
        self.disconnect_flag.set()

    def is_connected(self):
        return not self.disconnect_flag.is_set() and self.nano is not None

    def is_connecting(self):
        return (self.nano is not None) and (not self.nano.is_valid_device())
