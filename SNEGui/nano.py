import time
import serial

import logging

logger = logging.getLogger(__name__)

def read_until_condition(ser, condition, timeout, printer=None):
    end = time.monotonic() + timeout
    while end > time.monotonic():
        l = ser.readline().decode("ascii").strip()
        if printer:
            printer(l)
        if condition(l):
            break;

def strip_quotes(s):
    if s:
        return s[1:-1]
    return None

class SerialNano:

    SERIAL_URL_PARAM_NO = 1
    DEFAULT_RELAY_STATES_PARAM_NO = 2
    NAME_PARAM_NO = 3

    def __init__(self, serial_port):
        try:
            self.port = serial.Serial(serial_port, '115200', timeout=0.1)
        except OSError:
            self.port = None
            raise Exception(f"Could not open {serial_port}")

        self.reset_value = None
        self.name = None
        self.ip = None
        self.nrelays = 0
        self.outputs = []

    def wait_for_ready(self):
        read_until_condition(self.port, lambda l: l.endswith("ready"), 6)

    def is_valid_device(self):
        return (self.nrelays > 0) and (self.name is not None) and (self.reset_value is not None)

    def get_from_url(self, url, lines_before_reply):
        logger.info(f"Requesting data from URL {url}")

        self.set_param(self.SERIAL_URL_PARAM_NO, url)
        self.port.readline() # Reject "Handling URL... " line

        for _ in range(lines_before_reply):
            self.port.readline()

        reply = self.port.readline().decode("ascii").strip()

        # Reject two \r\n empty lines
        self.port.readline()
        self.port.readline()

        logger.info(f"Got reply {reply}")

        return reply

    def query_device(self, device_no, reply_lines):
        query = f"D{device_no:02}?\n".encode("ascii")
        self.port.write(query)
        replies = [self.port.readline().decode("ascii").strip() for _ in range(reply_lines)]
        return replies

    def update_ip(self):
        reply = self.query_device(1, 3)
        ip_reply = reply[0]

        if ip_reply.startswith("ENC: IP: "):
            ip_address = reply[0][9:]
        else:
            ip_address = None

        return ip_address

    def update(self):
        self.wait_for_ready()

        reset_value = self.get_param(self.DEFAULT_RELAY_STATES_PARAM_NO)
        if reset_value:
            self.reset_value = strip_quotes(reset_value)
            self.nrelays = len(self.reset_value)

        name = self.get_param(self.NAME_PARAM_NO)
        if name:
            self.name = strip_quotes(name)

        ip = self.update_ip()
        if ip:
            self.ip = ip

        if self.is_valid_device():
            logger.info(f"Found Nano '{self.name}' at '{self.ip}' with {self.nrelays} relays and reset value {self.reset_value}")
        else:
            logger.info(f"Device at '{self.port.name}' not a valid nano")

        if self.is_valid_device():
            self.update_relay_state()

    def update_relay_state(self):
            outputs_state_str = self.get_from_url("/outputs/states", 0)[5:].strip()
            self.outputs = [c == '1' for c in outputs_state_str]

    def close(self):
        if self.port:
            self.port.close()

    def set_param(self, param_no, setting):
        param_string = f"P{param_no:02}S{setting}\n".encode("ascii")
        self.port.write(param_string)
        reply = self.port.readline().decode("ascii").strip()
        if reply == f"P{param_no:02}>OK":
            logger.info(f"Parameter P{param_no:02} set to {setting}")
        else:
            logger.info(f"Parameter P{param_no:02} not set (got reply {reply})")

    def get_param(self, param_no):

        self.port.write(f"P{param_no:02}?\n".encode("ascii"))
        reply = self.port.readline().strip().decode("ascii")
        expected_start = f"P{param_no:02}>"
        if reply.startswith(expected_start):
            reply = reply[4:]
            logger.info(f"Get param {param_no} result: {reply}")
            return reply
        else:
            logger.info(f"Unexpected reply: {reply}")
            return None

    def set_ip(self, ip):
        set_ip_str = f"D01SIP{ip}\n".encode("ascii")
        print(set_ip_str)
        self.port.write(set_ip_str)
        reply = self.port.readline().decode("ascii").strip()
        if reply == f"D01>OK":
            logger.info(f"IP set to {ip}")
        else:
            logger.info(f"IP not set (got reply {reply})")

    def toggle_relay(self, relay_no):
        self.get_from_url(f"/output/toggle/{relay_no+3}", 1)

    def get_output_state(self, idx):
        try:
            return self.outputs[idx]
        except IndexError:
            logger.error(f"Index {idx} >= {self.nrelays}")
            return False

    def set_defaults(self):
        param_string = "".join(['1' if c else '0' for c in self.outputs])
        print(param_string)
        print(len(param_string))
        self.set_param(self.DEFAULT_RELAY_STATES_PARAM_NO, param_string)

    def __del__(self):
        self.close()
