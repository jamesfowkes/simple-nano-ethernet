from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need
# fine tuning.
build_options = {'packages': [], 'excludes': []}

import sys
base = 'Win32GUI' if sys.platform=='win32' else None

executables = [
    Executable('app.py', base=base, target_name = 'sne')
]

setup(name='Simple Nano Ethernet GUI',
      version = '1.0',
      description = 'An interface to configure the simple nano ethernet board',
      options = {'build_exe': build_options},
      executables = executables)
