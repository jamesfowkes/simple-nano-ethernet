import sys

import logging

sys.path.append("/home/james/code/cave-escape/simple-nano-ethernet/SNEGui/build/exe.linux-x86_64-3.8/lib/PySide6")

from PySide6 import QtCore, QtWidgets, QtGui

from ConnectGUI import ConnectGUI
from NanoGUI import NanoGUI
from ConnectionManager import ConnectionManager
from PortManager import PortManager

logger = logging.getLogger(__name__)

class MainGUI(QtWidgets.QMainWindow):

    def add_spacer(self):
        spacer = QtWidgets.QSpacerItem(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.main_layout.addItem(spacer)

    def __init__(self, port_manager, connection_manager):

        super().__init__()
        self.setWindowTitle('Simple Nano Ethernet Tester/Configurator')
        self.nano = None

        ## Widgets and layout
        self.main_widget = QtWidgets.QWidget()
        self.main_layout = QtWidgets.QVBoxLayout()
        self.main_widget.setLayout(self.main_layout)
        self.setCentralWidget(self.main_widget)

        self.connect_GUI = ConnectGUI(self.main_layout, port_manager, connection_manager)
        self.nano_GUI = NanoGUI(self.main_layout, connection_manager)
        #self.add_spacer()

        logger.info("GUI setup done")

if __name__ == "__main__":

    logging.basicConfig(level=logging.INFO)

    app = QtWidgets.QApplication(sys.argv)

    port_manager = PortManager()
    port_manager.start()

    connection_manager = ConnectionManager()
    connection_manager.start()

    gui = MainGUI(port_manager, connection_manager)
    gui.show()

    res = app.exec()

    port_manager.stop()
    port_manager.join()

    connection_manager.stop()
    connection_manager.join()

    sys.exit(res)
