import logging

import serial.tools.list_ports as lp

from StoppableThread import StoppableThread

logger = logging.getLogger(__name__)

class PortManager(StoppableThread):

    def __init__(self):
        StoppableThread.__init__(self)
        self.on_update_signal = None

    def run(self):
        while self.run_flag:
            if self.on_update_signal:
                self._busy = True
                logger.info("Refreshing serial ports")
                self.ports = lp.comports()
                logger.info(f"Got {len(self.ports)} ports")
                self._busy = False
                self.on_update_signal.emit(self.ports)
                self.on_update_signal = False

    def update(self, on_update_signal):
        self.on_update_signal = on_update_signal

    def is_busy(self):
        return self._busy

    def num_ports(self):
        return len(self.ports)

    def has_ports(self):
        return len(self.ports) > 0
