import sys

import logging

from PySide6 import QtCore, QtWidgets, QtGui

from nano import SerialNano
from IPV4Validator import IPV4Validator

logger = logging.getLogger(__name__)

class NanoGUI(QtWidgets.QWidget):

    def __init__(self, container, connection_manager):

        super().__init__()
        self.container = container

        ## Widgets and layout
        self.relay_box = QtWidgets.QGroupBox("Relay Control")
        self.relay_layout = QtWidgets.QGridLayout()
        self.relay_box.setLayout(self.relay_layout)

        self.ip_box = QtWidgets.QGroupBox("IP Address")
        self.ip_layout = QtWidgets.QGridLayout()
        self.ip_box.setLayout(self.ip_layout)

        # Connect signals and slots
        connection_manager.register_slot(self.on_connection_change)
        container.addWidget(self.relay_box)
        container.addWidget(self.ip_box)

        self.nano = None
        self.relay_box.setHidden(True)
        self.ip_box.setHidden(True)

        logger.info("Nano widget setup done")

    def clear_layout(self):
        for i in reversed(range(self.relay_layout.count())):
            self.relay_layout.itemAt(i).widget().setParent(None)
        for i in reversed(range(self.ip_layout.count())):
            self.ip_layout.itemAt(i).widget().setParent(None)

    def on_relay_button_click(self, i):
        if self.nano:
            self.nano.toggle_relay(i)
        self.nano.update_relay_state()
        if self.nano.get_output_state(i):
            self.relay_buttons[i].setStyleSheet("background-color: green")
        else:
            self.relay_buttons[i].setStyleSheet("background-color: red")

    def on_save_default_state(self):
        self.nano.set_defaults()

    def change_ip_button_clicked(self):
        self.nano.set_ip(self.ip_input.text())

    @QtCore.Slot(SerialNano)
    def on_connection_change(self, nano):

        self.clear_layout()
        self.relay_buttons = []

        self.nano = nano

        self.relay_box.setHidden(nano is None)
        self.ip_box.setHidden(nano is None)

        if nano:
            for i in range(nano.nrelays):
                wid = QtWidgets.QPushButton()
                if nano.get_output_state(i):
                    wid.setStyleSheet("background-color: green")
                else:
                    wid.setStyleSheet("background-color: red")
                wid.clicked.connect(lambda *args, arg=i: self.on_relay_button_click(arg))
                wid.setText(f"Relay D{i+3}")
                self.relay_layout.addWidget(wid, 0, i)
                self.relay_buttons.append(wid)

            self.save_button = QtWidgets.QPushButton()
            self.save_button.setText("Save as default relay state")
            self.save_button.clicked.connect(self.on_save_default_state)
            self.relay_layout.addWidget(self.save_button, 1, 0, 1, nano.nrelays)

            # IP address entry label
            ip_label = QtWidgets.QLabel()
            ip_label.setText("IP Address: ")
            ip_label.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed))

            self.ip_input = QtWidgets.QLineEdit()
            self.ip_input.setValidator(IPV4Validator())
            if nano.ip:
                self.ip_input.setText(self.nano.ip)

            self.ip_layout.addWidget(ip_label, 0, 0)
            self.ip_layout.addWidget(self.ip_input, 0, 1)

            # Serial port refresh button
            self.change_ip_button = QtWidgets.QPushButton()
            self.change_ip_button.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed))
            self.change_ip_button.clicked.connect(self.change_ip_button_clicked)
            self.change_ip_button.setText("Set IP")
            self.ip_layout.addWidget(self.change_ip_button, 0, 2)
