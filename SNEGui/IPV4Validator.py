from PySide6 import QtGui

class IPV4Validator(QtGui.QValidator):
    def __init__(self, parent=None):
        super(IPV4Validator, self).__init__(parent)

    def validate(self, address, pos):
        if not address:
            return QtGui.QValidator.Acceptable, address, pos
        octets = address.split(".")
        size = len(octets)
        if size > 4:
            return QtGui.QValidator.Invalid, address, pos
        emptyOctet = False
        for octet in octets:
            if not octet:
                emptyOctet = True
                continue
            try:
                value = int(str(octet))
            except ValueError:
                return QtGui.QValidator.Invalid, address, pos
            if value < 0 or value > 255:
                return QtGui.QValidator.Invalid, address, pos
        if size < 4 or emptyOctet:
            return QtGui.QValidator.Intermediate, address, pos
        return QtGui.QValidator.Acceptable, address, pos
