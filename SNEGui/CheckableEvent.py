import threading

class CheckableEvent(threading.Event):

    def __init__(self):
        threading.Event.__init__(self)
        self.lock = threading.Lock()

    def set(self):
        with self.lock:
            threading.Event.set(self)

    def check_and_clear(self):
        with self.lock:
            set = self.is_set()
            self.clear()

        return set
