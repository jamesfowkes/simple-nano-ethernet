import threading

class StoppableThread(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)
        self.run_flag = True

    def stop(self):
        self.run_flag = False
