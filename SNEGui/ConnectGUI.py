import sys

import logging

from PySide6 import QtCore, QtWidgets, QtGui

from nano import SerialNano

logger = logging.getLogger(__name__)

class ConnectGUI(QtWidgets.QWidget):

    ports_updated_signal = QtCore.Signal(list)

    def __init__(self, container, port_manager, connection_manager):

        super().__init__()
        self.port_manager = port_manager
        self.connection_manager = connection_manager
        self.wait_cursor = False

        ## Widgets and layout
        self.box = QtWidgets.QGroupBox("Arduino Connection")
        self.layout = QtWidgets.QGridLayout()
        self.box.setLayout(self.layout)

        # Serial port selection dropdown
        label = QtWidgets.QLabel()
        label.setText("Select serial port: ")
        label.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed))

        self.serial_select_box = QtWidgets.QComboBox()
        self.serial_select_box.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed))

        # Serial port refresh button
        self.serial_refresh_button = QtWidgets.QPushButton()
        self.serial_refresh_button.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed))
        pixmapi = getattr(QtWidgets.QStyle, "SP_BrowserReload")
        icon = self.style().standardIcon(pixmapi)
        self.serial_refresh_button.setIcon(icon);
        self.serial_refresh_button.clicked.connect(self.on_serial_refresh_click)

        # Serial port connect button
        self.serial_connect_button = QtWidgets.QPushButton()
        self.serial_connect_button.setText("Connect")
        self.serial_connect_button.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed))
        self.serial_connect_button.setCheckable(True)
        self.serial_connect_button.clicked.connect(self.on_serial_connect_click)
        self.serial_connect_button.setEnabled(False)

        # Connected Arduino information
        self.arduino_databox = QtWidgets.QLabel()
        self.arduino_databox.setText("No relay board connected")
        self.arduino_databox.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum))

        # Window layout
        self.layout.addWidget(label, 0, 0)
        self.layout.addWidget(self.serial_select_box, 0, 1)
        self.layout.addWidget(self.serial_refresh_button, 0, 2)
        self.layout.addWidget(self.serial_connect_button, 0, 3)
        self.layout.addWidget(self.arduino_databox, 1, 0)
        container.addWidget(self.box)

        # Connect signals and slots
        self.ports_updated_signal.connect(self.on_serial_dev_update)
        self.connection_manager.register_slot(self.on_serial_dev_connect)

        # Start by immediately refreshing serial ports
        self.port_manager.update(self.ports_updated_signal)

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.refresh)
        self.timer.start(250)

        logger.info("Connect widget setup done")

    def on_serial_refresh_click(self):
        self.port_manager.update(self.ports_updated_signal)

    def on_serial_connect_click(self):
        if self.connection_manager.is_connected():
            self.connection_manager.disconnect_nano()
        else:
            self.show_wait_cursor()
            self.connection_manager.connect_nano(self.serial_select_box.currentData())

    def show_wait_cursor(self):
        if not self.wait_cursor:
            self.wait_cursor = True
            QtWidgets.QApplication.setOverrideCursor(QtGui.QCursor(QtCore.Qt.WaitCursor))

    def refresh(self):

        still_connecting = False
        if self.port_manager.is_busy():
            self.serial_refresh_button.setEnabled(False)
            self.serial_connect_button.setEnabled(False)
        else:
            if self.connection_manager.is_connecting():
                self.show_wait_cursor()
                still_connecting = True
                self.serial_refresh_button.setEnabled(False)
                self.serial_connect_button.setEnabled(False)
            elif self.connection_manager.is_connected():
                self.serial_refresh_button.setEnabled(False)
                self.serial_connect_button.setEnabled(True)
                self.serial_connect_button.setChecked(True)
            else:
                self.serial_refresh_button.setEnabled(True)
                self.serial_connect_button.setChecked(False)
                self.serial_connect_button.setEnabled(self.port_manager.has_ports())

        if not still_connecting and self.wait_cursor:
            self.wait_cursor = False
            QtWidgets.QApplication.restoreOverrideCursor()

    @QtCore.Slot(list)
    def on_serial_dev_update(self, ports):
        logger.info(f"Refreshed with {len(ports)} serial ports")
        self.serial_select_box.clear()
        for port in ports:
            text = f"{port.name} ({port.description})"
            self.serial_select_box.addItem(text, port.device)

        if len(ports) == 0:
            self.connection_manager.disconnect_nano()

    @QtCore.Slot(SerialNano)
    def on_serial_dev_connect(self, nano):
        if nano:
            self.arduino_databox.setText(f"Connected: {nano.name} ({nano.nrelays}-relay board)")
        if not nano:
            self.arduino_databox.setText(f"No relay board connected")
        self.arduino_databox.repaint()
        self.repaint()
